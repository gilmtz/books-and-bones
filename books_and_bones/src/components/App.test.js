import React from "react";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import "isomorphic-fetch";
import waitUntil from 'async-wait-until';

//component imports
import Libraries from "./Libraries.js";
import Museums from "./Museums.js";
import Events from "./Events.js";
import LibraryInstance from "./LibraryInstance.js"
import MuseumInstance from "./MuseumInstance.js";
import EventInstance from "./EventInstance.js";

Enzyme.configure({ adapter: new Adapter() });

//GRID PAGE TESTS

describe('Library Grid Page',()=>{
	it('Successful render', () => {
		const wrapper = Enzyme.shallow(<Libraries />);
		expect(wrapper.exists()).toBe(true);
	})

	it('Count', async (done) => {
		const wrapper = Enzyme.shallow(<Libraries />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);
		expect(wrapper.state()).toEqual({
      		"instancesPerPage": 9,
        	"data": [], 
	    	"currentPage": null, 
       		"totalPages": null,
       		"total": 23,
       		refresh: true,
         	activeQueries: {},
         	activeSearch: "" 
     	});
		done();
	})

	it('Length of data', async (done) => {
		const wrapper = Enzyme.shallow(<Libraries />);
	
		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().onPageChanged(paginationData);
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"].length).toEqual(wrapper.state()["instancesPerPage"]);
		done();
	})

	it('Correct data fields', async (done) => {
		const wrapper = Enzyme.shallow(<Libraries />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['id']).toBeDefined();
		expect(wrapper.state()["data"][0]['name']).toBeDefined();
		expect(wrapper.state()["data"][0]['description']).toBeDefined();
		expect(wrapper.state()["data"][0]['district']).toBeDefined();
		expect(wrapper.state()["data"][0]['contact_info']).toBeDefined();
		expect(wrapper.state()["data"][0]['open_hours']).toBeDefined();
		expect(wrapper.state()["data"][0]['latitude']).toBeDefined();
		expect(wrapper.state()["data"][0]['longitude']).toBeDefined();
		expect(wrapper.state()["data"][0]['events']).toBeDefined();
		expect(wrapper.state()["data"][0]['image_url']).toBeDefined();
		done();
	})

	it('Filter by Zipcode', async (done) => {
		const wrapper = Enzyme.shallow(<Libraries />);
		await waitUntil(()=>wrapper.state()["total"] !== undefined);
		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateFilter('contact_info__address_zip','78701')
		await waitUntil(()=>wrapper.state()["activeQueries"]["zip"] != undefined)
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['contact_info'][0]['address_zip']).toBe('78701');
		done();
	})

	it('Filter by Zipcode + District', async (done) => {
		const wrapper = Enzyme.shallow(<Libraries />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateFilter('contact_info__address_zip','78702')
		wrapper.instance().updateFilter('district', '1')
		await waitUntil(()=>wrapper.state()["activeQueries"]["district"] != undefined)
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length == 1);
		expect(wrapper.state()["data"][0]['contact_info'][0]['address_zip']).toBe('78702');
		expect(wrapper.state()["data"][0]['district']).toBe('1');
		expect(wrapper.state()["data"][0]['name']).toBe('Carver Branch');
		done();
	})

	it('Search', async (done) => {
		const wrapper = Enzyme.shallow(<Libraries />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateSearch('Walnut')
		await waitUntil(()=>wrapper.state()["activeSearch"] != "");
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["total"]).toBe(1);
		expect(wrapper.state()["data"][0]['name']).toBe('Little Walnut Creek Branch');
		done();
	})


	it('Sort by name', async (done) => {
		const wrapper = Enzyme.shallow(<Libraries />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateFilter('Sort by Name', "Ascending")
		await waitUntil(()=>wrapper.state()["activeQueries"] != {})
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['name']).toBe('Austin History Center');
		done();
	})

})

describe('Museums Grid Page',()=>{

	it('Successful render', () => {
		const wrapper = Enzyme.shallow(<Museums />);
		expect(wrapper.exists()).toBe(true);
	})

	it('Count', async (done) => {
		const wrapper = Enzyme.shallow(<Museums />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);
		expect(wrapper.state()).toEqual({
      		"instancesPerPage": 9,
        	"data": [], 
	    	"currentPage": null, 
       		"totalPages": null,
       		"total": 250,
       		"refresh": true, 
     		"activeQueries": {},
        	"activeSearch": '' 
     	});
		done();
	})

	it('Length of data', async (done) => {
		const wrapper = Enzyme.shallow(<Museums />);
	
		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().onPageChanged(paginationData);
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"].length).toEqual(wrapper.state()["instancesPerPage"]);
		done();
	})

	it('Correct data fields', async (done) => {
		const wrapper = Enzyme.shallow(<Museums />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['id']).toBeDefined();
		expect(wrapper.state()["data"][0]['name']).toBeDefined();
		expect(wrapper.state()["data"][0]['summary']).toBeDefined();
		expect(wrapper.state()["data"][0]['mus_type']).toBeDefined();
		expect(wrapper.state()["data"][0]['contact_info']).toBeDefined();
		expect(wrapper.state()["data"][0]['image_url']).toBeDefined();
		done();
	})

		it('Filter by City', async (done) => {
		const wrapper = Enzyme.shallow(<Museums />);
		await waitUntil(()=>wrapper.state()["total"] !== undefined);
		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateFilter('contact_info__address_city','Alamo Heights')
		await waitUntil(()=>wrapper.state()["activeQueries"]["city"] != undefined)
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['contact_info'][0]['address_city']).toBe('Alamo Heights');
		expect(wrapper.state()["data"][0]['contact_info'][0]['address_county']).toBe('Bexar');
		done();
	})

	it('Filter by County + Type', async (done) => {
		const wrapper = Enzyme.shallow(<Museums />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateFilter('mus_type','Military')
		wrapper.instance().updateFilter('contact_info__address_county', 'Bexar')
		await waitUntil(()=>wrapper.state()["activeQueries"]["county"] != undefined)
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length == 3);
		expect(wrapper.state()["data"][0]['contact_info'][0]['address_city']).toBe('San Antonio');
		expect(wrapper.state()["data"][0]['contact_info'][0]['address_county']).toBe('Bexar');
		expect(wrapper.state()["data"][0]['mus_type']).toBe('Military');
		expect(wrapper.state()["data"][0]['name']).toBe('The Alamo');
		done();
	})

	it('Search', async (done) => {
		const wrapper = Enzyme.shallow(<Museums />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateSearch('Browning')
		await waitUntil(()=>wrapper.state()["activeSearch"] != "");
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["total"]).toBe(1);
		expect(wrapper.state()["data"][0]['name']).toBe('Armstrong Browning Library');
		done();
	})


	it('Sort by name', async (done) => {
		const wrapper = Enzyme.shallow(<Museums />);
		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateFilter('Sort by Name', "Descending")
		await waitUntil(()=>wrapper.state()["activeQueries"] != {})
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['name']).toBe('Yturri-Edmunds Historic Site');
		done();
	})
})

describe('Events Grid Page',()=>{

	it('Successful render', () => {
		const wrapper = Enzyme.shallow(<Events />);
		expect(wrapper.exists()).toBe(true);
	})

	it('Count', async (done) => {
		const wrapper = Enzyme.shallow(<Events />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);
		expect(wrapper.state()).toEqual({
      		"instancesPerPage": 9,
        	"data": [], 
	    	"currentPage": null, 
       		"totalPages": null,
       		"total": 259, 
       		"refresh": true, 
       		"activeQueries": {},
        	"activeSearch": '' 
     	});
		done();
	})

	it('Length of data', async (done) => {
		const wrapper = Enzyme.shallow(<Events />);
	
		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().onPageChanged(paginationData);
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"].length).toBeGreaterThan(1);
		done();
	})

	it('Correct data fields', async (done) => {
		const wrapper = Enzyme.shallow(<Events />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['id']).toBeDefined();
		expect(wrapper.state()["data"][0]['place']).toBeDefined();
		expect(wrapper.state()["data"][0]['place_name']).toBeDefined();
		expect(wrapper.state()["data"][0]['name']).toBeDefined();
		expect(wrapper.state()["data"][0]['description']).toBeDefined();
		expect(wrapper.state()["data"][0]['permalink']).toBeDefined();
		expect(wrapper.state()["data"][0]['pub_date']).toBeDefined();
		expect(wrapper.state()["data"][0]['event_image_url']).toBeDefined();
		done();
	})

	it('Filter by Host Institution', async (done) => {
		const wrapper = Enzyme.shallow(<Events />);
		await waitUntil(()=>wrapper.state()["total"] !== undefined);
		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateFilter('place_name','Central Library')
		await waitUntil(()=>wrapper.state()["activeQueries"]["place_name"] != undefined)
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['place_name']).toBe('Central Library');
		done();
	})

	it('Filter by Day + District', async (done) => {
		const wrapper = Enzyme.shallow(<Events />);

		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateFilter('day','Friday')
		wrapper.instance().updateFilter('place__district', '1')
		await waitUntil(()=>wrapper.state()["activeQueries"]["day"] != undefined)
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['pub_date']).toContain('Fri');
		done();
	})

	it('Search', async (done) => {
		const wrapper = Enzyme.shallow(<Events />);
		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateSearch('Book')
		await waitUntil(()=>wrapper.state()["activeSearch"] != "");
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		done();
	})


	it('Sort by name', async (done) => {
		const wrapper = Enzyme.shallow(<Events />);
		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateFilter('Sort by Name', "Ascending")
		await waitUntil(()=>wrapper.state()["activeQueries"] != {})
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['name']).toBe('ACCIO: Rubik&#039;s Cubing Club! - Ages 8-18');
		done();
	})

	it('Sort by date', async (done) => {
		const wrapper = Enzyme.shallow(<Events />);
		await waitUntil(()=>wrapper.state()["total"] !== undefined);

		const paginationData = {
      		currentPage: 1,
      		totalPages: 3,
      		pageLimit: wrapper.state()["instancesPerPage"],
      		totalRecords: wrapper.state()["total"]
    	};
		wrapper.instance().updateFilter('Sort by Date', "Descending")
		await waitUntil(()=>wrapper.state()["activeQueries"] != {})
		wrapper.instance().onPageChanged(paginationData)
		await waitUntil(()=>wrapper.state()["data"].length != 0);
		expect(wrapper.state()["data"][0]['name']).toBe('Citizenship 101 Classes - Tuesdays through April 30');
		done();
	})
})

///INSTANCE PAGE TESTS
describe('LibraryInstance',()=>{

	it('Successful render', () => {
		const wrapper = Enzyme.shallow(
			<LibraryInstance match={{params: {id: 1}, isExact: true, path: "", url: ""}} />);
		expect(wrapper.exists()).toBe(true);
	})


	it('Correct instance information a)', async (done) => {
		const wrapper = Enzyme.shallow(
			<LibraryInstance match={{params: {id: 1}, isExact: true, path: "", url: ""}} />);
	
		await waitUntil(()=>Object.keys(wrapper.state()).length != 0);
		expect(Object.keys(wrapper.state()).length).toBe(10)
		expect(wrapper.state()["id"]).toEqual(1);
		expect(wrapper.state()["name"]).toEqual("Little Walnut Creek Branch");
		done();
	})

	it('Correct instance information b)', async (done) => {
		const wrapper = Enzyme.shallow(
			<LibraryInstance match={{params: {id: 2}, isExact: true, path: "", url: ""}} />);
	
		await waitUntil(()=>Object.keys(wrapper.state()) != 0);
		expect(Object.keys(wrapper.state()).length).toBe(10)
		expect(wrapper.state()["id"]).toEqual(2);
		expect(wrapper.state()["name"]).toEqual("Carver Branch");
		done();
	})



})

describe('MuseumInstance',()=>{

	it('Successful render', () => {
		const wrapper = Enzyme.shallow(
			<MuseumInstance match={{params: {id: 1}, isExact: true, path: "", url: ""}} />);
		expect(wrapper.exists()).toBe(true);
	})


	it('Correct instance information a)', async (done) => {
		const wrapper = Enzyme.shallow(
			<MuseumInstance match={{params: {id: 763}, isExact: true, path: "", url: ""}} />);
	
		await waitUntil(()=>Object.keys(wrapper.state()).length != 0);

		expect(Object.keys(wrapper.state()).length).toBe(6)
		expect(wrapper.state()["id"]).toEqual(763);
		expect(wrapper.state()["name"]).toEqual("1st Cavalry Division Museum");
		expect(wrapper.state()["mus_type"]).toEqual("Military");
		done();
	})

	it('Correct instance information b)', async (done) => {
		const wrapper = Enzyme.shallow(
			<MuseumInstance match={{params: {id: 765}, isExact: true, path: "", url: ""}} />);
	
		await waitUntil(()=>Object.keys(wrapper.state()) != 0);
		expect(Object.keys(wrapper.state()).length).toBe(6)
		expect(wrapper.state()["id"]).toEqual(765);
		expect(wrapper.state()["name"]).toEqual("Agriculture Heritage Museum");
		expect(wrapper.state()["mus_type"]).toEqual("Agriculture");
		done();
	})

})

describe('EventInstance',()=>{

	it('Successful render', () => {
		const wrapper = Enzyme.shallow(
			<EventInstance match={{params: {id: 1}, isExact: true, path: "", url: ""}} />);
		expect(wrapper.exists()).toBe(true);
	})


	it('Instance exists', async (done) => {
		const wrapper = Enzyme.shallow(
			<EventInstance match={{params: {id: 931}, isExact: true, path: "", url: ""}} />);
		await waitUntil(()=>Object.keys(wrapper.state()).length > 1);
		expect(Object.keys(wrapper.state()).length).toBe(9)
		expect(wrapper.state()["id"]).toBeDefined();
		expect(wrapper.state()["place"]).toBeDefined();
		expect(wrapper.state()["place_name"]).toBeDefined();
		expect(wrapper.state()["pub_date"]).toBeDefined();
		done();
	})

})

