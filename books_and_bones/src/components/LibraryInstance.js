import React from "react";
import { Link } from "react-router-dom";
import Twitter from "./Twitter.js"
import HashTag from "./HashTag.js"
import MapWithAMarker from "./MapWithAMarker.js"
import RelatedEvents from "./RelatedEvents.js"

class LibraryInstance extends React.Component{
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    var targetUrl = 'https://api.booksandbones.me/libraries/'
    fetch(targetUrl + this.props.match.params.id)
      .then(response => response.json())
      .then(response => this.setState(response))
      .catch(err => console.error(this.props.url, err.toString()));
  }

  render(){
    if (this.state.events){
      return(
     <div className="container">
    <h1 class="mt-4 mb-3"><Link to={'/libraries/'}>Libraries</Link> / {this.state.name}</h1>

    <div class="row">

      <div class="col-md-8">
        <img class="img-fluid" src={this.state.image_url} alt=""></img>
      </div>

      <div class="col-md-4">
      {this.state.contact_info.map(function(item, index){
            return ( 
            <div>
              <h3 class="my-3">Contact</h3>
        <ul>
          <li>Phone: {item.phone}</li>
          <li>Website: <a href={item.website}> Here </a></li>
          <li>Address: {item.address_address}, {item.address_city}, {item.address_state}, {item.address_zip}</li>
        </ul></div>)
              })
            }

        <h3 class="my-3">Open Hours</h3>
        <ul>
        <FormatHours hours={this.state.open_hours}/>
        </ul>
      </div>

    </div>

    <HashTag twitter={"AustinLibrary"}/>

    <row>


      <h3 class="my-3">Library Description</h3>
      <p>{this.state.description}</p>
      <div class="row">
      <div class="col-md-4">
                  <Twitter twitter={"AustinLibrary"}/>
      </div>
      <div class="col-md-4">
      <MapWithAMarker lat={parseFloat(this.state.latitude)} lng={parseFloat(this.state.longitude)} />
      </div>
      </div>
    </row>
    <RelatedEvents place_id={this.state.id} />
    </div>
        );
      }
    return ( <div className="container">
    <h1 className="my-4">{this.state.name}</h1></div> )
  }
}


function FormatHours(props){

  var split_hours=props.hours.split(",")

    return(
      <div>
      {split_hours.map(function(day, index){
        if (!(day==="")){
        return(<li>{day}</li>)
      }
        return ("")
        })}
        <GetStatus sh={split_hours}/>
      </div>
      ) 

}

function GetStatus(props){
    var weekday_hours={}
    for (var week_idx in props.sh) {
        if(!(props.sh[week_idx]==="")){
        var hours = props.sh[week_idx].split("y:")[1]
        weekday_hours[week_idx]={}
        if (!(hours === " Closed")){
            var tokens=hours.split(" ")
            weekday_hours[week_idx]['start'] = parseInt(tokens[1].split(":")[0])
            weekday_hours[week_idx]['end'] = parseInt(tokens[4].split(":")[0]) + 12
        }
         }
    }

    var d = new Date();
    var cur_hour=d.getHours()

    if (cur_hour >= weekday_hours[d.getDay()]['start'] && cur_hour < weekday_hours[d.getDay()]['end']){
        return (<p className="card-text">Status: <font color="green">Open</font></p>)
    }
    return(<p className="card-text">Status: <font color="red">Closed</font></p>)
}

export default LibraryInstance;
