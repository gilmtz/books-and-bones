//modified from https://scotch.io/tutorials/build-custom-pagination-with-react

import React, { Component, Fragment } from 'react';

const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

/**
 * Helper method for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
}

class Pagination extends Component {

  constructor(props) {
    super(props);
    this.setup(props)

  }

  componentWillReceiveProps(nextProps) {
  	if (nextProps.refresh){
  		this.setup(nextProps);  
  	}
  }

  componentDidMount() {
    this.gotoPage(1);
  }

  setup(props){
  	const { totalRecords = null, pageLimit = 30, pageNeighbours = 0 } = props;

    this.pageLimit = typeof pageLimit === 'number' ? pageLimit : 30;
    this.totalRecords = typeof totalRecords === 'number' ? totalRecords : 0;

    // pageNeighbours can be: 0, 1 or 2
    this.pageNeighbours = typeof pageNeighbours === 'number'
      ? Math.max(0, Math.min(pageNeighbours, 2))
      : 0;

    this.totalPages = Math.ceil(this.totalRecords / this.pageLimit);
    this.state = { currentPage: 1 };
    this.gotoPage(1);
  }

  gotoPage = page => {
    const { onPageChanged = f => f } = this.props;

    const currentPage = Math.max(0, Math.min(page, this.totalPages));

    const paginationData = {
      currentPage,
      totalPages: this.totalPages,
      pageLimit: this.pageLimit,
      totalRecords: this.totalRecords,
      search: ""
    };

    this.setState({ currentPage }, () => onPageChanged(paginationData));
  }

  handleClick = page => evt => {
    evt.preventDefault();
    this.gotoPage(page);
  }

  /**
   * Let's say we have 10 pages and we set pageNeighbours to 2
   * Given that the current page is 6
   * The pagination control will look like the following:
   *
   * (1) < {4 5} [6] {7 8} > (10)
   *
   * (x) => terminal pages: first and last page(always visible)
   * [x] => represents current page
   * {...x} => represents page neighbours
   */
  fetchPageNumbers = () => {

    const totalPages = this.totalPages;
    const currentPage = this.state.currentPage;
    const pageNeighbours = this.pageNeighbours;

    if (currentPage == 1){
      return [currentPage, currentPage+1, totalPages]
    } else if (currentPage == totalPages)
      return [1, currentPage-1, totalPages]

    var range = [currentPage]
    if (currentPage > 2)
      range.unshift(currentPage-1)
    if (currentPage < totalPages-2)
      range.push(currentPage+1)
    range.unshift(1)
    range.push(totalPages)

    return range;

  }

  getLabel(page){
    switch(page){
      case 1:
        return "First";
      case this.totalPages:
        return "Last";
      case this.state.currentPage-1:
        return "Prev";
      case this.state.currentPage+1:
        return "Next";
        break;
      default:
        return page
    }
  }

  render() {

    if (!this.totalRecords || this.totalPages === 1){ 
      return null; }

    const { currentPage } = this.state;
    const pages = this.fetchPageNumbers();
    return (
      <Fragment>
          <ul className="pagination">
            { pages.map((page, index) => {

              return (
                <li key={index} className={`page-item${ currentPage === page ? ' active' : ''}`}>
                  <a className="page-link" href="#" onClick={ this.handleClick(page) }>{this.getLabel(page)}</a>
                </li>
              );

            }) }

          </ul>
      </Fragment>
    );

  }

}

export default Pagination;