import React from 'react';

class Twitter extends React.Component {

	constructor(props) {
		super(props);
		this.twitter = props.twitter;
	}

	render() {
		var url = "https://twitter.com/" + this.twitter +"?lang=en";
		var placeholder = "Tweets by " + this.twitter;
		return (
		  <div>
		  <a className="right" class="twitter-timeline" height="250px" width="250px"
		  href={url}>{placeholder}</a>
		  <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
		  </div>
		)
	}

}

export default Twitter;
