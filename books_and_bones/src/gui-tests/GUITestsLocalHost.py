from sys import platform
import sys
import unittest
import time
from unittest import TestCase
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC

class AcceptanceTests (TestCase):

	def setUp(self):
		self.homeurl = 'http://localhost:3000/'
		if platform == "linux" or platform == "linux2":
			self.browser = webdriver.Chrome('./drivers/chromedriver_linux')
		elif platform == "darwin": # macOS
			self.browser = webdriver.Chrome('./drivers/chromedriver_macosx')
		elif platform == "win32":
			self.browser = webdriver.Chrome('./drivers/chromedriver.exe')
		self.browser.get(self.homeurl)
		self.browser.maximize_window()

	# Tests for correct homepage
	def test1(self):
		self.browser.get(self.homeurl)
		self.assertIn('Books and Bones', self.browser.title)

	# Tests clickable home link on nav bar
	def test2(self):
		try:
			self.browser.find_element_by_link_text('Books and Bones').click()
			self.assertEqual("http://localhost:3000/", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	# Tests Books and Bones header is right
	def test3(self):
		self.browser.find_element_by_link_text('Books and Bones').click()
		given = self.browser.find_element_by_class_name("container").text
		self.assertIn('Books and Bones', given)

	# Tests libraries link works
	def test4(self):
		try:
			self.browser.find_element_by_link_text("libraries").click()
			self.assertEqual("http://localhost:3000/libraries", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	# Tests Libraries header is right
	def test5(self):
		self.browser.find_element_by_link_text('libraries').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('Libraries', given)

	# Tests museums link works
	def test6(self):
		try:
			self.browser.find_element_by_link_text('museums').click()
			self.assertEqual("http://localhost:3000/museums", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	# Tests Museum header is right
	def test7(self):
		self.browser.find_element_by_link_text('museums').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('Museums', given)

	# Tests events link works
	def test8(self):
		try:
			self.browser.find_element_by_link_text('events').click()
			self.assertEqual("http://localhost:3000/events", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	# Tests Events header is right
	def test9(self):
		self.browser.find_element_by_link_text('events').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('Events', given)

	# Tests about link works
	def test10(self):
		try:
			self.browser.find_element_by_link_text('about').click()
			self.assertEqual("http://localhost:3000/about", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	# Tests  About header is correct
	def test11(self):
		self.browser.find_element_by_link_text('about').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('About Books and Bones', given)


	# Tests gitlab link works
	def test12(self):
		self.browser.find_element_by_link_text('about').click()
		try:
			self.browser.find_element_by_id("GitLab Repository").click()
			self.assertEqual("https://gitlab.com/bstroud/books-and-bones", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	# Tests libraries link works
	def test13(self):
		try:
			self.browser.find_element_by_link_text("Libraries").click()
			self.assertEqual("http://localhost:3000/libraries", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	# Tests Libraries header is right
	def test14(self):
		self.browser.find_element_by_link_text('Libraries').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('Libraries', given)

	# Tests museums link works
	def test15(self):
		try:
			self.browser.find_element_by_link_text('Museums').click()
			self.assertEqual("http://localhost:3000/museums", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	# Tests Museum header is right
	def test16(self):
		self.browser.find_element_by_link_text('Museums').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('Museums', given)

	# Tests events link works
	def test17(self):
		try:
			self.browser.find_element_by_link_text('Events').click()
			self.assertEqual("http://localhost:3000/events", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	# Tests Events header is right
	def test18(self):
		self.browser.find_element_by_link_text('Events').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('Events', given)

	# Tests about link works
	def test19(self):
		try:
			self.browser.find_element_by_link_text('About').click()
			self.assertEqual("http://localhost:3000/about", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	# Tests  About header is correct
	def test20(self):
		self.browser.find_element_by_link_text('About').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('About Books and Bones', given)

	#Test search all page works
	def test21(self):
		self.browser.find_element_by_id("nav")
		time.sleep(1)
		try:
			searchBar = self.browser.find_element_by_id("searchbar")
			searchBar.click()
			searchBar.send_keys("branch")
			self.browser.find_element_by_id('searchbarbutton').click()
			self.assertEqual("http://localhost:3000/searchall/?search=branch", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")

	def endBrowser(self):
		self.browser.quit()

if __name__ == "__main__":
	unittest.main()
