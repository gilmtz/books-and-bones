from rest_framework import serializers
from api.models import Contact_info, Library, Event, Museum


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact_info
        fields = (
            "phone",
            "website",
            "address_address",
            "address_city",
            "address_state",
            "address_zip",
            "address_county",
        )


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = (
            "id",
            "place",
            "place_name",
            "name",
            "description",
            "permalink",
            "pub_date",
            "event_image_url",
            "date",
        )


class LibrarySerializer(serializers.ModelSerializer):
    contact_info = ContactSerializer(many=True, read_only=True)
    events = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Library
        fields = (
            "id",
            "name",
            "description",
            "district",
            "contact_info",
            "open_hours",
            "latitude",
            "longitude",
            "events",
            "image_url",
        )


class MuseumSerializer(serializers.ModelSerializer):
    contact_info = ContactSerializer(many=True, read_only=True)

    class Meta:
        model = Museum
        fields = ("id", "name", "summary", "mus_type", "contact_info", "image_url")
